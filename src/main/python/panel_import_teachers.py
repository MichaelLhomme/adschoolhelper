# This Python file uses the following encoding: utf-8

from PySide2 import QtCore, QtWidgets

from data.user import User
from data.teacher import Teacher

from tools.ui.import_table_model import ImportTableModel


class PanelImportTeachers(QtWidgets.QWidget):

    # Initialize the panel
    def __init__(self):
        super(PanelImportTeachers, self).__init__()

        from ui_panel_import_teachers import Ui_PanelImportTeachers
        self.ui = Ui_PanelImportTeachers()
        self.ui.setupUi(self)

        self.model = ImportTableModel(User.TEACHER_PATH, self)
        self.ui.tableView.setModel(self.model)

        self.ui.syncButton.clicked.connect(self.model.execute)

        self.model.processing_started.connect(lambda: self.ui.syncButton.setEnabled(False))
        self.model.processing_finished.connect(lambda: self.ui.syncButton.setEnabled(True))

        self.timer = QtCore.QTimer(self)
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.update_name_filter)

        self.ui.nameFilterEdit.textEdited.connect(self.on_name_filter_update)
        self.ui.actionFilterCombo.currentIndexChanged.connect(self.model.update_action_filter)


    # Forward is_operation_running to the model
    def is_operation_running(self):
        return self.model.is_operation_running()


    # Set the model with CSV data to import
    def set_csv_data(self, csv_data, csv_columns):
        (headers, rows) = csv_data
        teachers = []
        for row in rows:
            t = Teacher(row[csv_columns[0]], row[csv_columns[1]], row[csv_columns[2]])
            t.gen_username()
            teachers.append(t)

        self.model.set_import_users(teachers)


    # Delay the update of the name filter update
    def on_name_filter_update(self, filter):
        self.filter = filter
        self.timer.start(300)


    # Apply the name filter on the model
    def update_name_filter(self):
        self.model.update_name_filter(self.filter)
