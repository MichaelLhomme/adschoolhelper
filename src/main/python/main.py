from fbs_runtime.application_context.PySide2 import ApplicationContext
from PySide2.QtWidgets import QDialog

from connect_dialog import ConnectDialog
from mainwindow import MainWindow

import sys

if __name__ == '__main__':
    appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext

    # Open the connection dialog, then the main window if connection is successful
    if ConnectDialog().exec_() == QDialog.Accepted:
        window = MainWindow()

        exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
        sys.exit(exit_code)
