# This Python file uses the following encoding: utf-8

from data.user import User
from base_tab import BaseTab

from ui_tab_students import Ui_TabStudents

class TabStudents(BaseTab):

    # AD query to retrieve students
    query = {
        'where_clause': "objectClass = 'user'",
        'path': User.STUDENT_PATH
    }


    # Init panel
    def __init__(self):
        super(TabStudents, self).__init__(TabStudents.query, Ui_TabStudents())

        self.model.groups_updated.connect(self.ui.groupsFilterCombo.update_groups)
        self.ui.groupsFilterCombo.currentTextChanged.connect(self.update_group_filter)


    # Handle classes filter
    def update_group_filter(self, group):
        self.sortModel.update_group_filter(group)
