# This Python file uses the following encoding: utf-8

import qtawesome as qta
from PySide2 import QtCore, QtWidgets

from tools.ui.query_table_model import QueryTableModel
from tools.ui.sort_table_model import SortTableModel

class BaseTab(QtWidgets.QWidget):


    # Init panel
    def __init__(self, query, ui):
        super(BaseTab, self).__init__()

        self.ui = ui
        self.ui.setupUi(self)

        self.ui.refreshButton.setIcon(qta.icon('mdi.refresh', color = 'green'))
        self.ui.refreshButton.clicked.connect(self.refresh)

        # Init query tools
        self.filter = ""
        self.timer = QtCore.QTimer(self)
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.filter_name)

        self.ui.filterEdit.textEdited.connect(self.on_name_filter_update)


        # Init table and models
        self.model = QueryTableModel()
        self.sortModel = SortTableModel(self.model)
        self.ui.tableView.setModel(self.sortModel)

        self.sortModel.processing_started.connect(lambda: self.ui.refreshButton.setEnabled(False))
        self.sortModel.processing_finished.connect(lambda: self.ui.refreshButton.setEnabled(True))


    # Initialize the panel on show
    def on_show(self):
        if self.model.rowCount() == 0:
            self.refresh()


    # Refresh the view by querying AD
    def refresh(self):
        self.model.search(self.query)


    # Delay name filtering
    def on_name_filter_update(self, filter):
        self.filter = filter
        self.timer.start(500)


    # Update the model with a name filter
    def filter_name(self):
        self.sortModel.update_name_filter(self.filter)
