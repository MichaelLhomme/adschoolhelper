# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'tab_home.ui',
# licensing of 'tab_home.ui' applies.
#
# Created: Tue Nov 10 12:33:06 2020
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_TabHome(object):
    def setupUi(self, TabHome):
        TabHome.setObjectName("TabHome")
        TabHome.resize(549, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(TabHome)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(TabHome)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.createUserButton = QtWidgets.QToolButton(self.groupBox)
        self.createUserButton.setIconSize(QtCore.QSize(32, 32))
        self.createUserButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.createUserButton.setAutoRaise(False)
        self.createUserButton.setObjectName("createUserButton")
        self.horizontalLayout.addWidget(self.createUserButton)
        self.importTeachersButton = QtWidgets.QToolButton(self.groupBox)
        self.importTeachersButton.setIconSize(QtCore.QSize(32, 32))
        self.importTeachersButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.importTeachersButton.setAutoRaise(False)
        self.importTeachersButton.setObjectName("importTeachersButton")
        self.horizontalLayout.addWidget(self.importTeachersButton)
        self.importStudentsButton = QtWidgets.QToolButton(self.groupBox)
        self.importStudentsButton.setIconSize(QtCore.QSize(32, 32))
        self.importStudentsButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.importStudentsButton.setAutoRaise(False)
        self.importStudentsButton.setObjectName("importStudentsButton")
        self.horizontalLayout.addWidget(self.importStudentsButton)
        self.exportPasswordsButton = QtWidgets.QToolButton(self.groupBox)
        self.exportPasswordsButton.setIconSize(QtCore.QSize(32, 32))
        self.exportPasswordsButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.exportPasswordsButton.setAutoRaise(False)
        self.exportPasswordsButton.setObjectName("exportPasswordsButton")
        self.horizontalLayout.addWidget(self.exportPasswordsButton)
        self.verticalLayout.addWidget(self.groupBox)
        self.groupBox_2 = QtWidgets.QGroupBox(TabHome)
        self.groupBox_2.setObjectName("groupBox_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBox_2)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.quickFilterEdit = QtWidgets.QLineEdit(self.groupBox_2)
        self.quickFilterEdit.setClearButtonEnabled(True)
        self.quickFilterEdit.setObjectName("quickFilterEdit")
        self.verticalLayout_2.addWidget(self.quickFilterEdit)
        self.tableView = UsersTableView(self.groupBox_2)
        self.tableView.setObjectName("tableView")
        self.verticalLayout_2.addWidget(self.tableView)
        self.verticalLayout.addWidget(self.groupBox_2)

        self.retranslateUi(TabHome)
        QtCore.QMetaObject.connectSlotsByName(TabHome)

    def retranslateUi(self, TabHome):
        TabHome.setWindowTitle(QtWidgets.QApplication.translate("TabHome", "Form", None, -1))
        self.groupBox.setTitle(QtWidgets.QApplication.translate("TabHome", "Gestion des données", None, -1))
        self.createUserButton.setText(QtWidgets.QApplication.translate("TabHome", "Créer un utilisateur", None, -1))
        self.importTeachersButton.setText(QtWidgets.QApplication.translate("TabHome", "Importer des enseignants", None, -1))
        self.importStudentsButton.setText(QtWidgets.QApplication.translate("TabHome", "Importer des élèves", None, -1))
        self.exportPasswordsButton.setText(QtWidgets.QApplication.translate("TabHome", "Exporter les mots de passe", None, -1))
        self.groupBox_2.setTitle(QtWidgets.QApplication.translate("TabHome", "Recherche rapide", None, -1))
        self.quickFilterEdit.setPlaceholderText(QtWidgets.QApplication.translate("TabHome", "Filtrer", None, -1))

from tools.ui.users_table_view import UsersTableView
