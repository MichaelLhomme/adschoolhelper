# This Python file uses the following encoding: utf-8

from pyad import adcontainer # Required to prevent an import error with pywintypes
import qtawesome as qta
from PySide2 import QtCore, QtWidgets

from data.user import User
from data.student import Student

from tools.ad import init_thread, container_exists, get_container
from tools.worker import Worker


class Check():

    def __init__(self, icon, name, parent = None):
        self.icon = icon
        self.name = name
        self.parent = parent
        self.exists = False


    def get_path(self):
        if self.parent:
            return f"OU={self.name},{self.parent}"
        else:
            return f"OU={self.name}"


class TabADTree(QtWidgets.QWidget):

    tree_updated = QtCore.Signal()


    # Initialize the panel
    def __init__(self):
        super(TabADTree, self).__init__()

        from ui_tab_ad_tree import Ui_TabADTree
        self.ui = Ui_TabADTree()
        self.ui.setupUi(self)

        self.ui.createTreeButton.clicked.connect(self.create_tree)

        self.threadpool = QtCore.QThreadPool()

        self.checks = [
            Check(self.ui.iconUsers, User.ROOT_NAME),
            Check(self.ui.iconTeachers, User.TEACHER_NAME, User.ROOT_PATH),
            Check(self.ui.iconStudents, User.STUDENT_NAME, User.ROOT_PATH),
            Check(self.ui.iconStudents6, Student.CLASS_6_NAME, User.STUDENT_PATH),
            Check(self.ui.iconStudents5, Student.CLASS_5_NAME, User.STUDENT_PATH),
            Check(self.ui.iconStudents4, Student.CLASS_4_NAME, User.STUDENT_PATH),
            Check(self.ui.iconStudents3, Student.CLASS_3_NAME, User.STUDENT_PATH),
            Check(self.ui.iconStudents2, Student.CLASS_2_NAME, User.STUDENT_PATH),
            Check(self.ui.iconStudents1, Student.CLASS_1_NAME, User.STUDENT_PATH),
            Check(self.ui.iconStudentsT, Student.CLASS_T_NAME, User.STUDENT_PATH),
            Check(self.ui.iconStudentsBTS, Student.CLASS_BTS_NAME, User.STUDENT_PATH)
        ]


    # Check the AD Tree
    def check_ad_tree(self):
        self.start_processing()
        res = True

        for c in self.checks:
            if self.check_OU(c.icon, c.get_path()):
                c.exists = True
            else:
                res = False

        self.end_processing()
        return res


    # Check a given OU and update the related icon accordingly
    def check_OU(self, icon, OU):
        exists = container_exists(OU)
        self.update_icon(icon, exists)
        return exists


    # Update an icon with OK or error depending on status
    def update_icon(self, icon, status = True):
        if status:
            icon.setPixmap(qta.icon('mdi.check-circle', color = 'green').pixmap(24, 24))
        else:
            icon.setPixmap(qta.icon('mdi.alert-circle', color = 'red').pixmap(24, 24))


    # Start a worker to create the missing OUs
    def create_tree(self):
        worker = Worker(self.worker_handler)
        worker.signals.finished.connect(self.end_processing)
        worker.signals.error.connect(self.processing_error)

        self.start_processing()
        self.threadpool.start(worker)


    # Thread callback looping over the checks and creating the missing OUs
    def worker_handler(self, progress_callback):
        init_thread()

        for c in self.checks:
            if not c.exists:
                self.create_container(c)

        self.tree_updated.emit()


    # Create a missing OU
    def create_container(self, check):
        parent = get_container(check.parent)
        parent.create_container(check.name)


    # Update the UI with a processing state
    def start_processing(self):
        self.ui.progressBar.setMaximum(0)
        self.ui.createTreeButton.setEnabled(False)


    # Update the UI after a processing state
    def end_processing(self):
        self.ui.progressBar.setMaximum(100)
        self.ui.createTreeButton.setEnabled(True)


    # Handle processing errors
    def processing_error(self, error):
        (type, value, stack) = error
        self.end_processing()
        QtWidgets.QMessageBox.critical(self, "Erreur lors de la création de la structure", f"{type}: {value}")


    # Nothing to do on show
    def on_show(self):
        None
