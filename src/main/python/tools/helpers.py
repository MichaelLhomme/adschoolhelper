# This Python file uses the following encoding: utf-8

import os
import re
import codecs
import encodings

from PySide2.QtCore import QFile
from PySide2.QtUiTools import QUiLoader

from tools.ui.actions_filter_combobox import ActionsFilterComboBox
from tools.ui.groups_filter_combobox import GroupsFilterComboBox
from tools.ui.users_table_view import UsersTableView
from tools.ui.import_users_table_view import ImportUsersTableView


# Custom UI Loader
class UiLoader(QUiLoader):
    def createWidget(self, className, parent=None, name=""):
        if className == "UsersTableView":
            return UsersTableView(parent=parent)

        elif className == "ImportUsersTableView":
            return ImportUsersTableView(parent=parent)

        elif className == "ActionsFilterComboBox":
            return ActionsFilterComboBox(parent=parent)

        elif className == "GroupsFilterComboBox":
            return GroupsFilterComboBox(parent=parent)

        else:
            return super().createWidget(className, parent, name)


# Helper to load a Qt UI file
def load_ui_file(reference):
    ui_file_name = re.sub(r"\.py$", ".ui", reference)

    loader = UiLoader()

    path = os.path.join(os.path.dirname(reference), ui_file_name)
    ui_file = QFile(path)
    ui_file.open(QFile.ReadOnly)
    ui = loader.load(ui_file)
    ui_file.close()
    return ui


# List codecs for file encoding
def list_codecs():
    return [
        "ASCII",
        "UTF-8",
        "UTF-16",
        "UTF-32",
        "Big5",
        "GB2312",
        "EUC-TW",
        "HZ-GB-2312",
        "ISO-2022-CN",
        "EUC-JP",
        "SHIFT_JIS",
        "CP932",
        "ISO-2022-JP",
        "EUC-KR",
        "ISO-2022-KR",
        "KOI8-R",
        "MacCyrillic",
        "IBM855",
        "IBM866",
        "ISO-8859-5",
        "windows-1251",
        "ISO-8859-5",
        "windows-1251",
        "ISO-8859-1",
        "windows-1252",
        "ISO-8859-7",
        "windows-1253",
        "ISO-8859-8",
        "windows-1255",
        "TIS-620",
    ]
