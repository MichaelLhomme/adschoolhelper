# This Python file uses the following encoding: utf-8

from PySide2 import QtCore
from PySide2 import QtWidgets

import qtawesome as qta


# Custom widget for users table view
# Include a status label, a progress bar and a cancel button
class UsersTableView(QtWidgets.QWidget):

    # Initialize the widget
    def __init__(self, parent = None):
        super(UsersTableView, self).__init__(parent)

        self.menu = QtWidgets.QMenu(self)
        self.menu.addAction(qta.icon('mdi.account-key'), "Réinitialiser le mot de passe", self.on_reset_passwords)
        self.menu.addAction(qta.icon('mdi.account-remove'), "Supprimer", self.on_delete_users)
        self.menu.addAction(qta.icon('mdi.account-lock'), "Verrouiller", self.on_disable_users)
        self.menu.addAction(qta.icon('mdi.account-check'), "Déverrouiller", self.on_enable_users)

        self.table = QtWidgets.QTableView()
        self.table.setSortingEnabled(True)
        self.table.setAlternatingRowColors(True)
        self.table.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.table.customContextMenuRequested.connect(self.on_context_menu)
        self.table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        self.label = QtWidgets.QLabel()
        self.label.setText("aucun élément")
        self.label.setAlignment(QtCore.Qt.AlignRight)

        self.progressBar = QtWidgets.QProgressBar()
        self.progressBar.setTextVisible(False)
        self.progressBar.setVisible(False)

        self.cancelButton = QtWidgets.QToolButton()
        self.cancelButton.setIcon(qta.icon('mdi.cancel', color = 'red'))
        self.cancelButton.setVisible(False)
        self.cancelButton.clicked.connect(self.on_cancel)

        statusBar = QtWidgets.QWidget()
        sb_layout = QtWidgets.QHBoxLayout()
        sb_layout.addWidget(self.label)
        sb_layout.addWidget(self.progressBar)
        sb_layout.addWidget(self.cancelButton)
        statusBar.setLayout(sb_layout)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.table)
        layout.addWidget(statusBar)
        self.setLayout(layout)


    # Set the table's model and connect slots
    def setModel(self, model):
        model.modelReset.connect(self.update_status_label)

        model.processing_started.connect(self.start_loading)
        model.processing_progress.connect(self.update_progress)
        model.processing_finished.connect(self.loading_finished)

        self.table.setModel(model)


    # Update the status label with the current count of users
    def update_status_label(self):
        count = self.table.model().rowCount()
        text = "aucun élément" if count == 0 else f'{count} élément(s)'
        self.label.setText(text)


    # Get a list of currently selected users
    def selected_users(self):
        users = []
        model = self.table.model()

        for index in self.table.selectionModel().selectedRows():
            users.append(model.get_user(index))

        return users


    # Set the UI in loading mode
    def start_loading(self, progress):
        # self.table.setEnabled(False)
        self.label.setVisible(False)
        self.cancelButton.setVisible(progress)
        self.progressBar.setMaximum(100 if progress else 0)
        self.progressBar.setVisible(True)


    # Update the load progress
    def update_progress(self, i):
        self.progressBar.setValue(i)


    # Reset the UI after loading mode
    def loading_finished(self):
        # self.table.setEnabled(True)
        self.label.setVisible(True)
        self.cancelButton.setVisible(False)
        self.progressBar.setVisible(False)
        self.progressBar.setValue(0)
        self.progressBar.setMaximum(100)

        self.update_status_label()


    # Call cancel on the model
    def on_cancel(self):
        self.table.model().cancel_operation()


    # Handle right click for context menu
    def on_context_menu(self, point):
        self.menu.setEnabled(not self.table.model().is_operation_running())
        self.menu.exec_(self.table.mapToGlobal(point))


    # Call reset_users_passwords on the model
    def on_reset_passwords(self):
        users = self.selected_users()
        res = QtWidgets.QMessageBox.warning(
            self,
            "Confirmer la réinitialisation des mots de passes",
            f"Vous êtes sur le point de réinitialiser {len(users)} mot(s) de passe, souhaitez vous poursuivre ?",
            QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel
            )

        if res == QtWidgets.QMessageBox.Ok:
            self.table.model().reset_users_passwords(users)


    # Call delete_users on the model
    def on_delete_users(self):
        users = self.selected_users()
        res = QtWidgets.QMessageBox.warning(
            self,
            "Confirmer la suppression des comptes",
            f"Vous êtes sur le point de supprimer {len(users)} compte(s) utilisateur, souhaitez vous poursuivre ?",
            QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel
            )

        if res == QtWidgets.QMessageBox.Ok:
            self.table.model().delete_users(users)


    # Call enable_users on the model
    def on_enable_users(self):
        users = self.selected_users()
        res = QtWidgets.QMessageBox.warning(
            self,
            "Confirmer le déverrouillage des comptes",
            f"Vous êtes sur le point de déverrouiller {len(users)} compte(s) utilisateur, souhaitez vous poursuivre ?",
            QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel
            )

        if res == QtWidgets.QMessageBox.Ok:
            self.table.model().enable_users(users)


    # Call disable_users on the model
    def on_disable_users(self):
        users = self.selected_users()
        res = QtWidgets.QMessageBox.warning(
            self,
            "Confirmer le verrouillage des comptes",
            f"Vous êtes sur le point de verrouiller {len(users)} compte(s) utilisateur, souhaitez vous poursuivre ?",
            QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel
            )

        if res == QtWidgets.QMessageBox.Ok:
            self.table.model().disable_users(users)
