# This Python file uses the following encoding: utf-8


from data.users_table_model import UsersTableModel

from tools import ad as AD
from tools.worker import Worker


# Table model for AD queries
class QueryTableModel(UsersTableModel):

    # Initialize the model
    def __init__(self, parent = None):
        super(QueryTableModel, self).__init__(parent)


    # Start a search query
    def search(self, ad_query):
        # Clear the model
        self.clear()

        # Start a thread for the query
        self.worker = Worker(self.run_query, ad_query)
        self.worker.signals.finished.connect(self.query_finished)
        self.worker.signals.result.connect(lambda users: self.set_users(users))

        self.processing_started.emit(False)
        self.threadpool.start(self.worker)


    # Handle query finished
    def query_finished(self):
        self.worker = None
        self.processing_finished.emit()


    # Thread callback to execute the query
    def run_query(self, parameters, progress_callback):
        AD.init_thread()        
        return AD.search_users(parameters['path'], parameters['where_clause'])
