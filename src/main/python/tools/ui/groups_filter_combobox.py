# This Python file uses the following encoding: utf-8

from PySide2.QtWidgets import QComboBox


# Custom combobox for filtering groups
class GroupsFilterComboBox(QComboBox):

    ALL = 'Toutes les classes'

    def __init__(self, parent = None):
        super(GroupsFilterComboBox, self).__init__(parent)
        self.addItem(GroupsFilterComboBox.ALL)


    def update_groups(self, groups):
        groups.sort()
        groups.insert(0, GroupsFilterComboBox.ALL)

        self.clear()
        self.addItems(groups)
