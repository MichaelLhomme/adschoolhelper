# This Python file uses the following encoding: utf-8

from PySide2 import QtCore

from tools.ui.groups_filter_combobox import GroupsFilterComboBox

# Sort proxy for table views
class SortTableModel(QtCore.QSortFilterProxyModel):

    # Proxy signals for the underlying user model
    processing_started = QtCore.Signal(bool)
    processing_finished = QtCore.Signal()
    processing_progress = QtCore.Signal(int)

    # Initialize the proxy model with the given user model
    def __init__(self, user_model, parent = None):
        super(SortTableModel, self).__init__(parent)
        self.setSourceModel(user_model)

        # Forward the signals from the underlying model
        user_model.processing_started.connect(self.processing_started)
        user_model.processing_progress.connect(self.processing_progress)
        user_model.processing_finished.connect(self.processing_finished)

        # Setup filtering
        self.name_filter = None
        self.group_filter = None


    # Retrieve the list of users currently in use in the filter model
    def get_local_users(self):
        local_users = []

        for i in range(self.rowCount()):
            local_users.append(self.get_user(self.index(i, 0)))

        return local_users


    # Update the model with a name filter
    def update_name_filter(self, filter):
        self.beginResetModel()
        self.name_filter = filter
        self.endResetModel()


    # Update the model with a group filter
    def update_group_filter(self, group):
        self.beginResetModel()
        self.group_filter = group
        self.endResetModel()


    # Implement row filtering
    def filterAcceptsRow(self, source_row, parent):
        # Early exit to avoid fetching the user for nothing
        if self.name_filter == None and self.group_filter == GroupsFilterComboBox.ALL:
            return True

        user = self.sourceModel().get_user(self.sourceModel().index(source_row, 0))
        return self.filter_group(user) and self.filter_name(user)


    # Filter a user based on a name
    def filter_name(self, user):
        # No filter
        if self.name_filter == None or self.name_filter == "":
            return True

        # Match found in firstname
        if self.name_filter in user.firstname:
            return True

        # Match found in lastname
        if self.name_filter in user.lastname:
            return True

        # Match found in username
        if self.name_filter in user.username:
            return True

        return False


    # Filter a user based on its group
    def filter_group(self, user):
        # No filter
        if self.group_filter == None or self.group_filter == GroupsFilterComboBox.ALL:
            return True

        return user.group == self.group_filter


    # Forward get_user to the underlying model
    def get_user(self, index):
        return self.sourceModel().get_user(self.mapToSource(index))


    # Forward get_action to the underlying model
    def get_action(self, user):
        return self.sourceModel().get_action(user)


    # Forward is_operation_running to the underlying model
    def is_operation_running(self):
        return self.sourceModel().is_operation_running()


    # Forward cancel_operation to the underlying model
    def cancel_operation(self):
        self.sourceModel().cancel_worker()


    # Forward reset_users_passwords to the underlying model
    def reset_users_passwords(self, users):
        self.sourceModel().reset_users_passwords(users)


    # Forward delete_users to the underlying model
    def delete_users(self, users):
        self.sourceModel().delete_users(users)


    # Forward enable_users to the underlying model
    def enable_users(self, users):
        self.sourceModel().enable_users(users)


    # Forward disable_users to the underlying model
    def disable_users(self, users):
        self.sourceModel().disable_users(users)
