# This Python file uses the following encoding: utf-8

from PySide2.QtCore import Qt, QAbstractTableModel, QModelIndex

class CSVTableModel(QAbstractTableModel):
    def __init__(self, parent = None):
        QAbstractTableModel.__init__(self, parent)
        self.reader = None
        self.rows = []
        self.headers = []

    def set_csv_reader(self, reader):
        self.beginResetModel()
        self.reader = reader

        rows = []
        headers = reader.__next__()
        for row in reader:
            rows.append(row)

        self.headers = headers
        self.rows = rows
        self.endResetModel()

        return headers

    def get_data(self):
        return (self.headers, self.rows)

    def rowCount(self, parent = QModelIndex()):
        return len(self.rows)

    def columnCount(self, parent = QModelIndex()):
        return len(self.headers)

    def data(self, index, role):
        if not index.isValid():
            return None

        if index.row() >= self.rowCount():
            return None

        if role == Qt.DisplayRole:
            return self.rows[index.row()][index.column()]
        else:
            return None


    def headerData(self, section, orientation, role = Qt.DisplayRole):
        if role != Qt.DisplayRole:
            return None

        if orientation == Qt.Horizontal:
            return self.headers[section]
        else:
            return None
