# This Python file uses the following encoding: utf-8

import sys
import traceback

import csv
import chardet
from pathlib import Path


from PySide2 import QtCore, QtWidgets
from tools.helpers import list_codecs

from .csv_table_model import CSVTableModel


# Panel for CSV import
class PanelCSVImport(QtWidgets.QWidget):

    importCSV = QtCore.Signal()


    # Initialize the panel
    def __init__(self, withClass = False, initial_file = None):
        super(PanelCSVImport, self).__init__()
        self.withClass = withClass

        from .ui_panel_csv_import import Ui_PanelCSVImport
        self.ui = Ui_PanelCSVImport()
        self.ui.setupUi(self)

        self.ui.filenameEdit.textEdited.connect(self.read_file)

        self.ui.separatorEdit.setText(',')
        self.ui.separatorEdit.textEdited.connect(self.read_file)

        self.ui.encodingComboBox.addItems(list_codecs())
        self.ui.encodingComboBox.setCurrentIndex(self.ui.encodingComboBox.findText('utf_8'))
        self.ui.encodingComboBox.currentTextChanged.connect(self.read_file)

        self.ui.openButton.clicked.connect(self.choose_file)
        self.ui.importButton.clicked.connect(self.importCSV)
        self.ui.importButton.setEnabled(False)

        self.ui.comboFirstName.currentIndexChanged.connect(self.update_button)
        self.ui.comboLastName.currentIndexChanged.connect(self.update_button)
        self.ui.comboID.currentIndexChanged.connect(self.update_button)
        self.ui.comboClass.currentIndexChanged.connect(self.update_button)

        if not self.withClass:
            self.ui.labelClass.setVisible(False)
            self.ui.comboClass.setVisible(False)

        self.model = CSVTableModel()
        self.ui.tableView.setModel(self.model)

        if initial_file:
            self.ui.filenameEdit.setText(initial_file)
            self.read_file()


    # Get the indexes for the CSV
    def get_columns_indexes(self):
        return [self.ui.comboID.currentIndex(), self.ui.comboFirstName.currentIndex(), self.ui.comboLastName.currentIndex(), self.ui.comboClass.currentIndex()]


    # Retrieve the data from the CSV model
    def get_csv_data(self):
        return self.model.get_data()


    # Choose a CSV file for importation
    def choose_file(self):
        (filename, filter) = QtWidgets.QFileDialog.getOpenFileName(self, "Ouvrir un fichier CSV", str(Path.home()), "*.csv")
        self.ui.filenameEdit.setText(filename)

        # Encoding detection
        raw = Path(filename).read_bytes()
        res = chardet.detect(raw)
        self.ui.encodingComboBox.setCurrentText(res['encoding'])

        # Update the UI with the file content
        self.read_file()


    # Update the import button state
    def update_button(self, index):
        status = self.ui.comboFirstName.currentIndex() != -1 and self.ui.comboLastName.currentIndex() != -1 and self.ui.comboID.currentIndex() != -1 and (not self.withClass or self.ui.comboClass.currentIndex() != -1)

        self.ui.importButton.setEnabled(status)


    # Read the CSV file
    def read_file(self):
        path = self.ui.filenameEdit.text()
        encoding = self.ui.encodingComboBox.currentText()
        separator = self.ui.separatorEdit.text()

        if separator == "" or separator == None or encoding == "" or encoding == None:
            return

        try:
            contents = Path(path).read_text(encoding = encoding)
            self.ui.plainTextEdit.setPlainText(contents)

            self.ui.comboFirstName.clear()
            self.ui.comboFirstName.setCurrentIndex(-1)

            self.ui.comboLastName.clear()
            self.ui.comboLastName.setCurrentIndex(-1)

            self.ui.comboID.clear()
            self.ui.comboID.setCurrentIndex(-1)

            self.ui.comboClass.clear()
            self.ui.comboClass.setCurrentIndex(-1)

            with open(path, 'rt', 1, encoding) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter = separator)
                headers = self.model.set_csv_reader(csv_reader)

                self.ui.comboFirstName.addItems(headers)
                self.ui.comboLastName.addItems(headers)
                self.ui.comboID.addItems(headers)
                self.ui.comboClass.addItems(headers)

        except:
            exctype, value = sys.exc_info()[:2]
            traceback.print_exc()
            QtWidgets.QMessageBox.critical(self, "Erreur d'ouverture du fichier", f'ERROR: {traceback.format_exception_only(exctype, value)[0].strip()}')
