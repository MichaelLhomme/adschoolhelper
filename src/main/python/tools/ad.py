# This Python file uses the following encoding: utf-8

import re

import pythoncom
from pyad import pyad, adcontainer, adquery, aduser

from data.user import User
from data.teacher import Teacher
from data.student import Student


# Singleton helper for root AD DN
class ADHolder:
    __instance = None

    @staticmethod
    def init(username, password):
        auth = {'username': username, 'password': password}
        root_dn = pyad.from_cn("Users", options = auth).get_domain().dn

        h = ADHolder.getInstance()
        h.root = root_dn
        h.auth = auth

    @staticmethod
    def getInstance():
        if ADHolder.__instance == None:
            ADHolder()
        return ADHolder.__instance

    def __init__(self):
        if ADHolder.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            ADHolder.__instance = self
            self.root = None
            self.auth = None


# Initialize pythoncom (needed from threads)
def init_thread():
    pythoncom.CoInitialize()


# Concatenate the root DN with the given path to create a full DN
def resolve_path(path):
    root_dn = ADHolder.getInstance().root

    if path:
        return f'{path},{root_dn}'
    else:
        return root_dn


# Get a container object from AD
def get_container(path):
    full_dn = resolve_path(path)
    return adcontainer.ADContainer.from_dn(full_dn, options = ADHolder.getInstance().auth)


# Check if a container exists
def container_exists(path):
    try:
        get_container(path)
        return True
    except:
        return False


# Check if a username is available
def username_available(username):
    return pyad.from_cn(username, options = ADHolder.getInstance().auth) == None


# Enforce unique username by incrementing a counter if needed
def enforce_unique_username(username, pending_usernames):
    if (not username in pending_usernames) and username_available(username):
        return username

    else:
        counter = 0
        generated_username = username

        # Loop until an available username is found
        while True:
            counter = counter + 1
            generated_username = f"{username}{counter}"
            if (not generated_username in pending_usernames) and username_available(generated_username):
                break

        return generated_username


# Get a user object from AD
def get_user(user):
    full_dn = resolve_path(user.ad_current_path())
    return aduser.ADUser.from_dn(full_dn, options = ADHolder.getInstance().auth)


# Delete a user from AD
def delete_user(user):
    u = get_user(user)
    u.delete()


# Reset a user's password in AD
def reset_user_password(user):
    u = get_user(user)
    u.set_password(user.password)
    u.force_pwd_change_on_login()


# Enable a user
def enable_user(user):
    u = get_user(user)
    u.enable()


# Disable a user
def disable_user(user):
    u = get_user(user)
    u.disable()


# Create a new user in AD
def create_user(user):
    if user.id == "":
        raise ValueError("ID manquant")

    OUContainer = get_container(user.ad_dir())

    attributes = {
        'givenName': user.firstname,
        'sn': user.lastname,
        'employeeID': user.id,
        'department': user.group
    }

    OUContainer.create_user(user.username, password = user.password, optional_attributes = attributes)


# Update a user in AD
def update_user(user, new_ou = None):
    ad_user = get_user(user)

    attributes = {
        'givenName': user.firstname,
        'sn': user.lastname,
        'department': user.group
    }

    ad_user.update_attributes(attributes)

    if new_ou:
        ad_user.move(get_container(new_ou))


# Search users in AD
def search_users(path, where_clause = None):
    full_dn = resolve_path(path)

    if where_clause == None:
        full_where_clause = "objectClass = 'user'"
    else:
        full_where_clause = f"objectClass = 'user' and {where_clause}"

    q = adquery.ADQuery(options = ADHolder.getInstance().auth)
    q.execute_query(
        attributes = ["distinguishedName", "cn", "employeeID", "department", "sn", "givenName", "userAccountControl"],
        where_clause = full_where_clause,
        base_dn = full_dn
    )

    users = []
    for row in q.get_results():
        dn = row['distinguishedName']
        relative_dn = re.sub("^[^,]*,", "", dn).replace(f",{resolve_path(None)}", "")

        if User.TEACHER_PATH in dn:
            user = Teacher(row['employeeID'], row['givenName'], row['sn'], row ['cn'])
        elif User.STUDENT_PATH in dn:
            user = Student(row['employeeID'], row['givenName'], row['sn'], row['department'], row ['cn'])
        else:
            raise(ValueError(f"DN d'utilisateur inconnu: {dn}"))

        user.current_ou = relative_dn
        if row['userAccountControl'] & pyad.pyadconstants.ADS_USER_FLAG['ACCOUNTDISABLE']:
            user.disabled = True

        users.append(user)

    return users
