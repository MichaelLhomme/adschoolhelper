# This Python file uses the following encoding: utf-8

from .user import User

class Teacher(User):

    def __init__(self, id, firstname, lastname, username = None, password = None):
        super(Teacher, self).__init__(id, firstname, lastname, "professeur", username, password)


    def ad_dir(self):
        return User.TEACHER_PATH
