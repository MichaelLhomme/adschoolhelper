# This Python file uses the following encoding: utf-8

import sys
import traceback

from PySide2 import QtCore, QtGui
from PySide2.QtCore import Qt, QModelIndex

from data.action import Action, ActionResetPassword, ActionDeleteUser, ActionEnableUser, ActionDisableUser
from tools.worker import Worker
from tools.ad import init_thread


COL_USERNAME = "Nom d'utilisateur"
COL_FIRSTNAME = "Prénom"
COL_LASTNAME = "Nom"
COL_ID = "ID"
COL_GROUP = "Groupe"


# Base table model for users
class UsersTableModel(QtCore.QAbstractTableModel):

    processing_started = QtCore.Signal(bool)
    processing_finished = QtCore.Signal()
    processing_progress = QtCore.Signal(int)

    groups_updated = QtCore.Signal(list)

    # Initialize the model
    def __init__(self, parent = None):
        super(UsersTableModel, self).__init__(parent)
        self.headers = [COL_USERNAME, COL_FIRSTNAME, COL_LASTNAME, COL_GROUP, COL_ID]

        self.users = []
        self.actions = {}

        self.worker = None
        self.stop_worker = False
        self.threadpool = QtCore.QThreadPool()


    # Clear the model
    def clear(self):
        self.beginResetModel()
        self.users = []
        self.actions = {}
        self.endResetModel()

        self.groups_updated.emit([])


    # Set the users for the model
    def set_users(self, users, new_actions = None):
        groups = {}

        self.beginResetModel()
        self.users = users
        self.actions = new_actions if new_actions else {}

        for user in users:
            groups[user.group] = True

        self.endResetModel()

        self.groups_updated.emit(list(groups.keys()))


    # Get a user by its index
    def get_user(self, index = QModelIndex()):
        if not index.isValid():
            return None
        else:
            return self.users[index.row()]


    # Get an action (if any) for the given user
    def get_action(self, user):
        return self.actions.get(user.id)


    # Implement QAbstractTableModel.rowCount
    def rowCount(self, parent = QModelIndex()):
        return len(self.users)


    # Implement QAbstractTableModel.columnCount
    def columnCount(self, parent = QModelIndex()):
        return len(self.headers)


    # Implement QAbstractTableModel.data
    def data(self, index, role):
        # Sanity checks
        if not index.isValid() or index.row() >= self.rowCount():
            return None

        # Handle action icons
        if role == Qt.DecorationRole and index.column() == 0:
            u = self.users[index.row()]
            a = self.actions.get(u.id)

            if a:
                return a.get_icon()
            else:
                return None

        # Handle disabled accounts
        if role == Qt.ForegroundRole:
            u = self.users[index.row()]
            if u.disabled:
                return QtGui.QColor('darkGray')
            else:
                return None

        # Handle tooltip for errors
        if role == Qt.ToolTipRole:
            u = self.users[index.row()]
            a = self.actions.get(u.id)

            if a:
                return a.message

        # Handle cell content
        if role == Qt.DisplayRole:
            if self.headers[index.column()] == COL_LASTNAME:
                return self.users[index.row()].lastname

            elif self.headers[index.column()] == COL_FIRSTNAME:
                return self.users[index.row()].firstname

            elif self.headers[index.column()] == COL_USERNAME:
                return self.users[index.row()].username

            elif self.headers[index.column()] == COL_ID:
                return self.users[index.row()].id

            elif self.headers[index.column()] == COL_GROUP:
                return self.users[index.row()].group

        else:
            return None


    # Implement QAbstractTableModel.headerData
    def headerData(self, section, orientation, role = Qt.DisplayRole):
        if role != Qt.DisplayRole:
            return None

        if orientation == Qt.Horizontal:
            return self.headers[section]
        else:
            return None


    # Helper function to force refresh the icons
    def notify_actions_changed(self):
        self.dataChanged.emit(self.createIndex(0, 0), self.createIndex(self.rowCount() - 1, 0))


    # Merge the given actions
    def set_actions(self, actions):
        for user_id in actions.keys():
            self.actions[user_id] = actions[user_id]
        self.notify_actions_changed()


    # Init actions
    def init_actions(self, users, action):
        for u in users:
            self.actions[u.id] = action(u)
        self.notify_actions_changed()

        # Start the thread
        self.start_worker()


    # Start a worker thread to reset the password of the given users
    def reset_users_passwords(self, users):
        self.init_actions(users, ActionResetPassword)


    # Start a worker thread to delete the given users
    def delete_users(self, users):
        self.init_actions(users, ActionDeleteUser)


    # Start a worker thread to enable the given users
    def enable_users(self, users):
        self.init_actions(users, ActionEnableUser)


    # Start a worker thread to disable the given users
    def disable_users(self, users):
        self.init_actions(users, ActionDisableUser)


    # Execute an action
    def execute_action(self, action):
        if action.status == Action.STATUS_DONE:
            return

        user = action.payload
        index = self.createIndex(self.users.index(user), 0)

        action.status = Action.STATUS_RUNNING
        self.dataChanged.emit(index, index)

        try:
            res = action.execute()
            action.status = Action.STATUS_DONE
        except:
            exctype, value = sys.exc_info()[:2]
            action.set_error(f'ERROR: {traceback.format_exception_only(exctype, value)[0].strip()}')

            print(f"Error processing {action} on {action.payload.username}:", flush = True)
            traceback.print_exc()

        self.dataChanged.emit(index, index)

        # Remove the user's row
        #if res == Action.ROW_DELETED:
        #    row = self.users.index(user)
        #    self.beginRemoveRows(QModelIndex(), row, row)
        #    self.actions.pop(user.id)
        #    self.users.remove(user)
        #    self.endRemoveRows()

        # Update the user's row
        #elif res == Action.ROW_UPDATED:
        #    self.actions.pop(user.id)
        #    index = self.createIndex(self.users.index(user), 0)
        #    self.dataChanged.emit(index, index)


    # Cancel a running worker
    def cancel_worker(self):
        self.stop_worker = True


    # Test if an operation is currently running
    def is_operation_running(self):
        return self.worker != None


    # Start a worker thread on the current list of actions
    def start_worker(self, users = None):
        if users == None or users == []:
            actions = list(self.actions.values())
        else:
            actions = []
            for u in users:
                if u.id in self.actions and self.actions[u.id]:
                    actions.append(self.actions[u.id])

        self.stop_worker = False
        self.worker = Worker(self.worker_handler, self.execute_action, actions)
        self.worker.signals.progress.connect(self.processing_progress)
        self.worker.signals.finished.connect(self.worker_finished)

        self.processing_started.emit(True)
        self.threadpool.start(self.worker)


    # Handle worker finished
    def worker_finished(self):
        self.worker = None
        self.processing_finished.emit()


    # Handle worker thread
    def worker_handler(self, hook, actions, progress_callback):
        init_thread()

        count = len(actions)
        for i, action in enumerate(actions):
            if self.stop_worker:
                break

            hook(action)
            progress_callback.emit((i + 1) * 100 / count)
