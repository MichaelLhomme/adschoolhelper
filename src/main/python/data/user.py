# This Python file uses the following encoding: utf-8

import re
import csv

from unidecode import unidecode
from passgen import passgen

class User:

    ROOT_NAME = "Utilisateurs"
    ROOT_PATH = f"OU={ROOT_NAME}"

    TEACHER_NAME = "Professeurs"
    TEACHER_PATH = f"OU={TEACHER_NAME},{ROOT_PATH}"

    STUDENT_NAME = "Eleves"
    STUDENT_PATH = f"OU={STUDENT_NAME},{ROOT_PATH}"

    pattern = re.compile('[\W_]+')

    def __init__(self, id, firstname, lastname, group, username = None, password = None):
        self.id = id.strip()
        self.firstname = firstname.strip()
        self.lastname = lastname.strip()
        self.group = group.strip() if group else None
        self.username = username.strip() if username else None
        self.password = password
        self.current_ou = None
        self.disabled = False


    def gen_password(self):
        #passgen(length=12, punctuation=False, digits=True, letters=True, case='both')
        self.password = passgen(length=8, punctuation=False, digits=True, letters=True, case='both')


    def gen_username(self):
        nf = User.pattern.sub('', unidecode(self.firstname)).lower()
        nl = User.pattern.sub('', unidecode(self.lastname)).lower()
        self.username = f'{nf[:3]}{nl[:5]}'


    def ad_path(self):
        return f'CN={self.username},{self.ad_dir()}'


    def ad_current_path(self):
        return f'CN={self.username},{self.current_ou}'
