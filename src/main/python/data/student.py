# This Python file uses the following encoding: utf-8

from .user import User

class Student(User):

    CLASS_6_NAME = "Sixiemes"
    CLASS_6_PATH = f"OU={CLASS_6_NAME}"

    CLASS_5_NAME = "Cinquiemes"
    CLASS_5_PATH = f"OU={CLASS_5_NAME}"

    CLASS_4_NAME = "Quatriemes"
    CLASS_4_PATH = f"OU={CLASS_4_NAME}"

    CLASS_3_NAME = "Troisiemes"
    CLASS_3_PATH = f"OU={CLASS_3_NAME}"

    CLASS_2_NAME = "Secondes"
    CLASS_2_PATH = f"OU={CLASS_2_NAME}"

    CLASS_1_NAME = "Premieres"
    CLASS_1_PATH = f"OU={CLASS_1_NAME}"

    CLASS_T_NAME = "Terminales"
    CLASS_T_PATH = f"OU={CLASS_T_NAME}"

    CLASS_BTS_NAME = "BTS"
    CLASS_BTS_PATH = f"OU={CLASS_BTS_NAME}"

    def __init__(self, id, firstname, lastname, group, username = None, password = None):
        super(Student, self).__init__(id, firstname, lastname, group, username, password)


    def ad_dir(self):
        f = self.group[0]

        if f == "6":
            return f"{Student.CLASS_6_PATH},{User.STUDENT_PATH}"
        elif f == "5":
            return f"{Student.CLASS_5_PATH},{User.STUDENT_PATH}"
        elif f == "4":
            return f"{Student.CLASS_4_PATH},{User.STUDENT_PATH}"
        elif f == "3":
            return f"{Student.CLASS_3_PATH},{User.STUDENT_PATH}"
        elif f == "2":
            return f"{Student.CLASS_2_PATH},{User.STUDENT_PATH}"
        elif f == "1":
            return f"{Student.CLASS_1_PATH},{User.STUDENT_PATH}"
        elif f == "T":
            if self.group.startswith("TPIL"):
                return f"{Student.CLASS_BTS_PATH},{User.STUDENT_PATH}"
            else:
                return f"{Student.CLASS_T_PATH},{User.STUDENT_PATH}"
        else:
            raise ValueError(f"Unknown student group {self.group}")
