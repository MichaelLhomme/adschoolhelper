# This Python file uses the following encoding: utf-8

class PasswordsExporter:
    __instance = None

    @staticmethod
    def getInstance():
        if PasswordsExporter.__instance == None:
            PasswordsExporter()
        return PasswordsExporter.__instance


    def __init__(self):
        if PasswordsExporter.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            PasswordsExporter.__instance = self
            self.users = {}
            self.saved = False


    @staticmethod
    def add_user(user):
        p = PasswordsExporter.getInstance()
        p.users[user.username] = user
        p.saved = False


    @staticmethod
    def get_users():
        return list(PasswordsExporter.getInstance().users.values())


    @staticmethod
    def set_saved():
        p = PasswordsExporter.getInstance()
        p.saved = True


    @staticmethod
    def is_saved():
        return PasswordsExporter.getInstance().saved
