# This Python file uses the following encoding: utf-8

import qtawesome as qta
from PySide2 import QtCore, QtWidgets

from data.action import Action, ActionCreateUser, ActionUpdateUser, ActionDeleteUser, ActionResetPassword, ActionEnableUser, ActionDisableUser


class DialogLegend(QtWidgets.QDialog):

    # Initialize the dialog
    def __init__(self):
        super(DialogLegend, self).__init__()

        # Setup UI and connect slots
        from ui_dialog_legend import Ui_DialogLegend
        self.ui = Ui_DialogLegend()
        self.ui.setupUi(self)


        self.ui.iconActionDone.setIcon(qta.icon('mdi.account', color = Action.COLOR_DONE))
        self.ui.iconActionWorking.setIcon(qta.icon('mdi.account', color = Action.COLOR_RUNNING))
        self.ui.iconActionError.setIcon(qta.icon('mdi.account', color = Action.COLOR_ERROR))

        self.set_icon(self.ui.iconActionCreate, ActionCreateUser)
        self.set_icon(self.ui.iconActionUpdate, ActionUpdateUser)
        self.set_icon(self.ui.iconActionDelete, ActionDeleteUser)
        self.set_icon(self.ui.iconActionEnable, ActionEnableUser)
        self.set_icon(self.ui.iconActionDisable, ActionDisableUser)
        self.set_icon(self.ui.iconActionResetPasswd, ActionResetPassword)


    def set_icon(self, button, actionClass):
        (icon, color) = actionClass.ICON
        button.setIcon(qta.icon(icon, color = color))
